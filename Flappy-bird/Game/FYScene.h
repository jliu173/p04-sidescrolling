//
//  Flappy-bird
//
//  Created by Jiangyun on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//


#define kStartGameNotification @"StartGameNotification"

#define kRetoPlayNotification @"RetoPlayNotification"

#define kOverToBackNotification @"OverToBackNotification"

/**
 游戏屏幕
 */
@interface FYScene : SKScene<SKPhysicsContactDelegate>

@property (nonatomic) NSInteger currentScore;


/**
 开始游戏
 */
- (void) startPlayGame;

@end
