//
//  Flappy-bird
//
//  Created by Jiangyun on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//
#import "FYScene.h"
#import "Nodes.h"


#define FYBACKSCROLLINGSPEED .5     //背景滚动速度
#define FYFLOORSCROLLINGSPEED 3     // 底部滚动速度

// Obstacles 障碍物的设置
#define FYVERTICAL_GAP_SIZE 140       // 障碍物上下间的距离,如果想要设置简单一些，就把数字设置大点
#define FYFIRST_PADDING 100      // 开始玩的时候，第一个障碍物的距离
#define FYMIN_HEIGHT 100          // 障碍物最小高度
#define FYINTERVAL_SPACE 160     // 障碍物的距离，如果要设置简单一些，就把数字设置大点


@interface FYScene () {
    FYScrollingNode * floorNode;
    FYScrollingNode * backNode;
    SKLabelNode * myScoreLabel;
    FYBirdNode * mybird;
    
    int obstaclesOfThis;
    NSMutableArray * topPipes;
    NSMutableArray * bottomPipes;
}

@end

@implementation FYScene

static bool misused = NO;

/// 初始化
- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.physicsWorld.contactDelegate = self;
        [self startPlayGame];
    }
    return self;
}

/**
 屏幕点击事件
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(!misused) {
        if (!mybird.physicsBody) {
            [mybird startFlyBird];
            [[NSNotificationCenter defaultCenter] postNotificationName:kRetoPlayNotification object:nil];
        }
        [mybird jump];
        
    } else {
        [self startPlayGame];
    }
}


#pragma delegate
/// 撞击障碍物的委托
- (void)didBeginContact:(SKPhysicsContact *)contact {
    if (misused) {
        return;
    }
    
    misused = true;
    
    if (self.currentScore > [[NSUserDefaults standardUserDefaults] integerForKey:kBestFlyScoreKey]) {
        [[NSUserDefaults standardUserDefaults] setInteger:self.currentScore forKey:kBestFlyScoreKey];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kOverToBackNotification object:nil];
}

/**
 开始玩
 */
- (void) startPlayGame {
    misused = NO;
    
    [self removeAllChildren];
    
    /// 初始化创建
    backNode = [FYScrollingNode createNodeWithImageNamed:@"back" inSizeWidth:self.frame.size.width];
    [backNode setScrollingSpeed:FYBACKSCROLLINGSPEED];
    [backNode setAnchorPoint:CGPointZero];
    [backNode setPhysicsBody:[SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame]];
    backNode.physicsBody.categoryBitMask = backSpritBitMask;
    backNode.physicsBody.contactTestBitMask = birdSpritBitMask;
    [self addChild:backNode];
    
    floorNode = [FYScrollingNode createNodeWithImageNamed:@"floor" inSizeWidth: self.frame.size.width];
    [floorNode setScrollingSpeed:FYFLOORSCROLLINGSPEED];
    [floorNode setAnchorPoint:CGPointZero];
    [floorNode setName:@"floor"];
    [floorNode setPhysicsBody:[SKPhysicsBody bodyWithEdgeLoopFromRect:floorNode.frame]];
    floorNode.physicsBody.categoryBitMask = floorSpritBitMask;
    floorNode.physicsBody.contactTestBitMask = birdSpritBitMask;
    [self addChild:floorNode];
    
    self.currentScore = 0;
    myScoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Helvetica-Bold"];
    myScoreLabel.text = @"0";
    myScoreLabel.fontSize = 60;
    myScoreLabel.alpha = 0.7;
    myScoreLabel.position = CGPointMake(CGRectGetMinX(self.frame) + 25, self.frame.size.height - 70);
    [self addChild:myScoreLabel];
    
    // 计算我们需要多少障碍，越少越好
    obstaclesOfThis = ceil((self.frame.size.width) / (FYINTERVAL_SPACE));
    
    CGFloat lastBlockPos = 0;
    bottomPipes = [NSMutableArray array];
    topPipes = [NSMutableArray array];
    for(int i = 0; i < obstaclesOfThis; i++) {
        
        SKSpriteNode * topPipe = [SKSpriteNode spriteNodeWithImageNamed:@"pipe_top"];
        [topPipe setAnchorPoint:CGPointZero];
        [self addChild:topPipe];
        [topPipes addObject:topPipe];
        
        SKSpriteNode * bottomPipe = [SKSpriteNode spriteNodeWithImageNamed:@"pipe_bottom"];
        [bottomPipe setAnchorPoint:CGPointZero];
        [self addChild:bottomPipe];
        [bottomPipes addObject:bottomPipe];
        
        // 在第一个障碍之前给玩家一些时间
        if(0 == i) {
            [self placeIn:bottomPipe and:topPipe atX:self.frame.size.width + FYFIRST_PADDING];
        } else {
            [self placeIn:bottomPipe and:topPipe atX:lastBlockPos + bottomPipe.frame.size.width + FYINTERVAL_SPACE];
        }
        lastBlockPos = topPipe.position.x;
    }
    
    mybird = [FYBirdNode new];
    [mybird setPosition:CGPointMake(100, CGRectGetMidY(self.frame))];
    mybird.name = @"bird";
    [self addChild:mybird];
    
    floorNode.zPosition = mybird.zPosition + 1;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kStartGameNotification object:nil];

}

/// 更新当前状态
- (void)update:(NSTimeInterval)time {
    if(misused) {
        return;
    }

    [backNode updatePostion:time];
    [floorNode updatePostion:time];
    
    [mybird updateState:time];
    [self updateNextObstacles:time];
    [self updateScore:time];
}


// 更新障碍物
- (void) updateNextObstacles:(NSTimeInterval)time {
    if (!mybird.physicsBody) {
        return;
    }
    
    for(int i = 0; i < obstaclesOfThis; i++) {
        // 获取管道
        SKSpriteNode * top = (SKSpriteNode *) topPipes[i];
        SKSpriteNode * bottom = (SKSpriteNode *) bottomPipes[i];
        
        // 检查是否已退出屏幕，并再次将其放置在前面
        if ( (top.frame.origin.x) < - GETWIDTH(top) ) {
            SKSpriteNode * mostRightPipe = (SKSpriteNode *) topPipes[(i+(obstaclesOfThis-1)) % obstaclesOfThis];
            [self placeIn:bottom and:top atX: (mostRightPipe.frame.origin.x) + GETWIDTH(top) + FYINTERVAL_SPACE];
        }
        
        // 根据滚动速度移动
        top.position = CGPointMake((top.frame.origin.x) - FYFLOORSCROLLINGSPEED, (top.frame.origin.y));
        bottom.position = CGPointMake((bottom.frame.origin.x) - FYFLOORSCROLLINGSPEED, (bottom.frame.origin.y));
    }
}



/// 更新分数
- (void) updateScore:(NSTimeInterval) time {
    for(int i = 0; i < obstaclesOfThis; i++) {
        
        SKSpriteNode * top = (SKSpriteNode *) topPipes[i];
        
        // 记录分数
        if (top.frame.origin.x + top.frame.size.width / 2 > mybird.position.x &&
            top.frame.origin.x + top.frame.size.width / 2 < mybird.position.x + FYFLOORSCROLLINGSPEED) {
            self.currentScore += 1;
            myScoreLabel.text = [NSString stringWithFormat:@"%lu",(long)self.currentScore]; // 更新text
        }
    }
}



#pragma private
/// 计算柱子
- (void) placeIn:(SKSpriteNode *) bottomPipe and:(SKSpriteNode *) topPipe atX:(float) xPos {
    
    float availableSpace = GETHEIGHT(self) - GETHEIGHT(floorNode);
    float maxVariance = availableSpace - (2*FYMIN_HEIGHT) - FYVERTICAL_GAP_SIZE;
    float variance = [self randomFloatValueBetween:0 and:maxVariance];
    
    // 底部管道放置
    float minBottomPosY = GETHEIGHT(floorNode) + FYMIN_HEIGHT - GETHEIGHT(self);
    float bottomPosY = minBottomPosY + variance;
    bottomPipe.position = CGPointMake(xPos,bottomPosY);
    bottomPipe.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(0,0, GETWIDTH(bottomPipe) , GETHEIGHT(bottomPipe))];
    bottomPipe.physicsBody.categoryBitMask = blockSpritBitMask;
    bottomPipe.physicsBody.contactTestBitMask = birdSpritBitMask;
    
    // 顶部管道放置
    topPipe.position = CGPointMake(xPos,bottomPosY + GETHEIGHT(bottomPipe) + FYVERTICAL_GAP_SIZE);
    topPipe.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(0,0, GETWIDTH(topPipe), GETHEIGHT(topPipe))];
    
    topPipe.physicsBody.categoryBitMask = blockSpritBitMask;
    topPipe.physicsBody.contactTestBitMask = birdSpritBitMask;
}

/**
 随机数
 */
- (float) randomFloatValueBetween:(float) min and:(float) max {
    return  ((arc4random()%RAND_MAX)/(RAND_MAX*1.0))*(max-min)+min;
}

@end
