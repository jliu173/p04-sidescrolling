
//  Flappy-bird
//
//  Created by Jiangyun on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//

@interface FYScrollingNode : SKSpriteNode

/**
 滚动速度
 */
@property (nonatomic) CGFloat scrollingSpeed;

// 根据相应图片和宽度创建实体
+ (id) createNodeWithImageNamed:(NSString *)name inSizeWidth:(float) width;

- (void) updatePostion:(NSTimeInterval)time;

@end




/**
 小鸟
 */
@interface FYBirdNode : SKSpriteNode
/// 更新状态
- (void) updateState:(NSUInteger) time;

/// 开始玩
- (void) startFlyBird;
- (void) jump;

@end
