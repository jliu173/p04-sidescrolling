//
//  Flappy-bird
//
//  Created by Momo on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//

#import "FYViewController.h"
#import "FYScene.h"

@interface FYViewController () {
    FYScene * gameScene;
}

@property (weak,nonatomic) IBOutlet SKView * birdGameView;
@property (weak,nonatomic) IBOutlet UIView * readyView;

@property (weak,nonatomic) IBOutlet UIView * gameOverView;
@property (weak,nonatomic) IBOutlet UILabel * currentScoreLabel;
@property (weak,nonatomic) IBOutlet UILabel * bestLabel;

@end

@implementation FYViewController

- (void)setting {
    // 创建配置游戏屏幕.
    gameScene = [FYScene sceneWithSize:self.birdGameView.bounds.size];
    gameScene.scaleMode = SKSceneScaleModeAspectFill;
    
    _gameOverView.layer.cornerRadius = 6;
    _gameOverView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _gameOverView.layer.borderWidth = 1;
    _gameOverView.layer.masksToBounds = YES;
    _gameOverView.alpha = 0;
    _gameOverView.transform = CGAffineTransformMakeScale(.9, .9);
    [_birdGameView presentScene:gameScene];
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];

    [self setting];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startGame) name:kStartGameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retoPlay) name:kRetoPlayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameOverGoBack) name:kOverToBackNotification object:nil];
}



/**
 开始游戏
 */
- (void)startGame {
    [UIView animateWithDuration:.2 animations:^{
        _gameOverView.alpha = 0;
        _gameOverView.transform = CGAffineTransformMakeScale(.8, .8);
        _readyView.alpha = 1;
    } completion:nil];
}

- (void)retoPlay {
    [UIView animateWithDuration:.5 animations:^{
        _readyView.alpha = 0;
    }];
}

/// 游戏回来的通知结果
- (void)gameOverGoBack {
    [UIView animateWithDuration:.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _gameOverView.alpha = 0.7;
        _gameOverView.transform = CGAffineTransformMakeScale(1, 1);

        _currentScoreLabel.text = [NSString stringWithFormat:@"%li", gameScene.currentScore]; // 当前分数
        _bestLabel.text = [NSString stringWithFormat:@"%li", (long)[[NSUserDefaults standardUserDefaults] integerForKey:kBestFlyScoreKey]];   // 最好成绩
        
    } completion: nil];
    
}


/**
 隐藏状态栏
 */
- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
