//
//  main.m
//  Flappy-bird
//
//  Created by Jiangyun on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
