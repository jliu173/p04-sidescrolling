//
//  Flappy-bird
//
//  Created by Jiangyun on 2017/3/13.
//  Copyright © 2017年 Vold. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
